# [PSI Internal Outing - Web ](https://startbootstrap.com/template-overviews/freelancer/)

[PSI Internal Outing - Web ](http://startbootstrap.com/template-overviews/freelancer/) is [Bootstrap](http://getbootstrap.com/) based used for  prize lottery web interface in Packet Systems Indonesia outing event in bali. Created by SSS Team.

## Preview

[![Freelancer Preview](https://startbootstrap.com/assets/img/templates/freelancer.jpg)](https://blackrockdigital.github.io/startbootstrap-freelancer/)

**[View Live Preview](https://blackrockdigital.github.io/startbootstrap-freelancer/)**

## Status

[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://raw.githubusercontent.com/BlackrockDigital/startbootstrap-freelancer/master/LICENSE)
[![npm version](https://img.shields.io/npm/v/startbootstrap-freelancer.svg)](https://www.npmjs.com/package/startbootstrap-freelancer)
[![Build Status](https://travis-ci.org/BlackrockDigital/startbootstrap-freelancer.svg?branch=master)](https://travis-ci.org/BlackrockDigital/startbootstrap-freelancer)
[![dependencies Status](https://david-dm.org/BlackrockDigital/startbootstrap-freelancer/status.svg)](https://david-dm.org/BlackrockDigital/startbootstrap-freelancer)
[![devDependencies Status](https://david-dm.org/BlackrockDigital/startbootstrap-freelancer/dev-status.svg)](https://david-dm.org/BlackrockDigital/startbootstrap-freelancer?type=dev)


## Copyright and License

Copyright 2018 Packet Systems Indonesia.
