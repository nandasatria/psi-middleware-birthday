# !/bin/bash

os_version=$(cat /etc/*release | head -1 | awk '{print toupper($0)}' | awk 'match($0, /CENTOS|UBUNTU|FEDORA/) {print substr($0, RSTART, RLENGTH)}')
printf "Operating System Varian : %s\n" $os_version

if [ $os_version = "UBUNTU" ];
then
    apt-get purge --auto-remove --yes redis redis-server openjdk-8-jdk apache2 git
    rm -rf /var/www/html/*
else
    yum -y remove git java-1.8.0-openjdk* redis redis-server httpd git
    rm -rf /var/www/html/*
fi
