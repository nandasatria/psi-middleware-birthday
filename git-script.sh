# !/bin/bash


os_version=$(cat /etc/*release | head -1 | awk '{print toupper($0)}' | awk 'match($0, /CENTOS|UBUNTU|FEDORA/) {print substr($0, RSTART, RLENGTH)}')
printf "Operating System Varian : %s\n" $os_version

printf "Update Repository\n"
if  [ $os_version = "UBUNTU" ];
then
    apt-get update
    printf "Update Repository....................................................%s\n" "[SUCCESS]" 
    printf "\n"
else
    yum -y update
    printf "Update Repository....................................................%s\n" "[SUCCESS]" 
    printf "\n"
fi

printf "Checking git client"
if [ -x "$(command -v git)" ];
then
    printf "Git Client Status........................................%s \n" "[ALREADY-INSTALLED]"
else
    printf "Git Client Status %60s \n" "[NOT-FOUND]" 
    printf "Install Git Client\n"
    if [ $os_version = "UBUNTU" ];
    then
        apt-get --yes install git
    else 
        yum -y install git
    fi
    printf "Git Client Instalation......................................%s \n" "[INSTALL-SUCCESS]"
fi

printf "Clone Git Data\n"
git clone https://gitlab.com/nandasatria/psi-middleware-birthday.git

cd psi-middleware-birthday
printf "Run Script\n"
bash run-apps.sh