# !/bin/bash

install_prerequisite()
{
    if  [ $1 = "UBUNTU" ];
    then
        printf "Update Repository\n"
        apt-get update
        printf "Update Repository....................................................%s\n" "[SUCCESS]" 
        printf "\n"
    else
        if [ "$(rpm -qa | grep epel)" = "" ] && [ $1 = "CENTOS" ];
        then 
            echo "Install Epel"
            yum install -y epel-release
            printf "Installation EPEL...........................................%s \n" "[INSTALL-SUCCESS]"
        fi
        
        printf "Update Repository\n"
        yum -y update
        printf "Update Repository....................................................%s\n" "[SUCCESS]" 
        printf "\n"
    fi
}

install_redis()
{   
    echo "Checking Redis"
    if [ -x "$(command -v redis-server)" ];
    then
        printf "Redis-Server Status........................................%s \n" "[ALREADY-INSTALLED]"
        if [ "$(redis-cli ping)" = "PONG" ];
        then
            printf "Redis Server.................................................%s \n" "[ALREADY-RUNNING]"
        else
            printf "Redis Server.....................................................%s \n" "[NOT-RUNNING]"
            echo "Running Redis Server..."
            redis-server >> redis-server.log &
            sleep 5
            printf "Redis Server..................................................%s \n" "[RUNNING-SUCCESS]"
            
        fi
    else
        printf "Redis-Server Status %60s \n" "[NOT-FOUND]" 
        echo "Install Redis-Server\n"
        if [ $1 = "UBUNTU" ];
        then 
            apt-get install --yes redis
            printf "Redis-Server Instalation......................................%s \n" "[INSTALL-SUCCESS]"
            echo "Running Redis Server..."
            systemctl start redis
        else 
            yum -y install redis
              printf "Redis-Server Instalation......................................%s \n" "[INSTALL-SUCCESS]"
            echo "Running Redis Server..."
            systemctl start redis
        fi 
    fi
    
    printf "Redis Info \n"
    redis-cli info | grep redis_version
    printf "\n\n"
}

install_java()
{
    echo "Checking Java"
    if [ -x "$(command -v java)" ];
    then
        printf "Java Status.................................................%s \n" "[ALREADY-INSTALLED]"
        java -version
    else
        printf "Java Status.........................................................%s \n" "[NOT-FOUND]" 
        printf "Installing Java...\n"
        if [ $1 = "UBUNTU" ];
        then 
            apt-get install --yes openjdk-8-jdk
        else 
            yum -y install java-1.8.0-openjdk
        fi
        printf "\n"
        printf "Java Instalation..............................................%s \n" "[INSTALL-SUCCESS]"
        java -version
    fi
    printf "\n\n"
}

install_httpd()
{
    echo "Checking Httpd"
    if [ $1 = "UBUNTU" ];    
    then
        if [ -x "$(command -v apache2)" ];
        then
            printf "Httpd Status ...............................................%s \n" "[ALREADY-INSTALLED]"
            apache2 -version
        else
            printf "Httpd Status........................................................%s \n" "[NOT-FOUND]" 
            printf "Installing Apache2 Httpd...\n"
            apt-get install --yes apache2
            printf "Httpd Instalation.............................................%s \n" "[INSTALL-SUCCESS]"
            apache2 -version
        fi
    else
        if [ -x "$(command -v httpd)" ];
        then
            printf "Httpd Status ...............................................%s \n" "[ALREADY-INSTALLED]"
            httpd -version
        else
            printf "Httpd Status........................................................%s \n" "[NOT-FOUND]" 
            printf "Installing Apache2 Httpd...\n"
            yum -y install httpd
            printf "Httpd Instalation.............................................%s \n" "[INSTALL-SUCCESS]"
            systemctl start httpd.service
            httpd -version
        fi
    fi
    printf "\n\n"
}

run_apps(){
    pid=$(ps -ef | grep psi-middleware-birthday-0.0.1-SNAPSHOT.jar | grep ' java ' | awk '{print $2}' | head -1)
    if [ "${pid}" = "" ];
    then 
        java -jar psi-middleware-birthday-0.0.1-SNAPSHOT.jar >> apps.log &
        echo "Running Application"
        sleep 12
        pid=$(ps -ef | grep psi-middleware-birthday-0.0.1-SNAPSHOT.jar | grep ' java ' | awk '{print $2}' | head -1)
        printf "\nApps Running In PID : %s" $pid
    else
        printf "Apps Already Running in PID : %s" $pid
    fi

    printf "\n\n"
}

load_data(){
    printf "Load Participant,..\n"
    curl -X POST 'http://localhost:8180/v2/loader'
}

load_ui_to_httpd(){
    cp -r psi-internal-outing-web/* /var/www/html/
    printf "\n\n"
    printf "Hooray Website Ready to Run !!!!"
    printf "\n\n"
}

os_version=$(cat /etc/*release | head -1 | awk '{print toupper($0)}' | awk 'match($0, /CENTOS|UBUNTU|FEDORA/) {print substr($0, RSTART, RLENGTH)}')

printf "Operating System Varian : %s\n" $os_version


install_prerequisite ${os_version}
install_redis ${os_version}
install_java ${os_version}
install_httpd ${os_version}
run_apps ${os_version}
load_data ${os_version}
load_ui_to_httpd

printf "\n\n"